package Task1;

import java.util.HashMap;
import java.util.Map;

public class RusPaper extends Thread{
    Map<String, String> dictionary = new HashMap<>();

    String nameTown = "";
    int temperature = 0;
    int pressure = 0;
    int humidity = 0;

    @Override
    public void run() {
        dictionary.put("Town", "Город");
        dictionary.put("Temperature", "Температура");
        dictionary.put("Pressure", "Давление");
        dictionary.put("Humidity", "Влажность");
        dictionary.put("Kharkov", "Харьков");
        while (true){
            if(!((Weather.getInstance().concurrentHashMap.get("Town")).equals(nameTown) && Weather.getInstance().concurrentHashMap.get("Temperature").equals(temperature) &&
                    Weather.getInstance().concurrentHashMap.get("Pressure").equals(pressure) && Weather.getInstance().concurrentHashMap.get("Humidity").equals(humidity))){
                nameTown = (String)Weather.getInstance().concurrentHashMap.get("Town");
                temperature = (int)Weather.getInstance().concurrentHashMap.get("Temperature");
                pressure = (int)Weather.getInstance().concurrentHashMap.get("Pressure");
                humidity = (int)Weather.getInstance().concurrentHashMap.get("Humidity");
                Weather.printWeather(dictionary.get("Town"), dictionary.get("Kharkov"), dictionary.get("Temperature"), dictionary.get("Pressure"), dictionary.get("Humidity"));

            }
        }
    }
}
