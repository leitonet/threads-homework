package Task1;

public class EnglishPaper extends Thread{

    String nameTown = "";
    int temperature = 0;
    int pressure = 0;
    int humidity = 0;

    @Override
    public void run() {
        while (true){
            if(!((Weather.getInstance().concurrentHashMap.get("Town")).equals(nameTown) && Weather.getInstance().concurrentHashMap.get("Temperature").equals(temperature) &&
                    Weather.getInstance().concurrentHashMap.get("Pressure").equals(pressure) && Weather.getInstance().concurrentHashMap.get("Humidity").equals(humidity))){
                nameTown = (String)Weather.getInstance().concurrentHashMap.get("Town");
                temperature = (int)Weather.getInstance().concurrentHashMap.get("Temperature");
                pressure = (int)Weather.getInstance().concurrentHashMap.get("Pressure");
                humidity = (int)Weather.getInstance().concurrentHashMap.get("Humidity");
                Weather.printWeather("Town","Kharkov" , "Temperature", "Pressure", "Humidity");

            }
        }
    }
}
