package Task1;

import java.util.Random;

public class Refresher extends Thread{

    @Override
    public void run() {
        while (true){
            Weather.refreshWeather("Kharkov", new Random().nextInt(40-5)+5, new Random().nextInt(300-50)+50, new Random().nextInt(90-20)+20);
            try {
                Thread.sleep(2_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
