package Task1;

import java.util.HashMap;
import java.util.Map;

public class Weather extends Thread {

    public static Map<String, Object> concurrentHashMap = new HashMap<>();
    private static Weather instance;

    public synchronized static Weather getInstance(){
        if(instance == null){
            instance = new Weather();
            return instance;
        }else {
            return instance;
        }
    }

    private Weather(){
    }

    public static synchronized void refreshWeather(String nameTown, int temperature, int pressure, int humidity){
        concurrentHashMap.put("Town", nameTown);
        concurrentHashMap.put("Temperature", temperature);
        concurrentHashMap.put("Pressure", pressure);
        concurrentHashMap.put("Humidity", humidity);
    }

    public static synchronized void printWeather(String nameTown, String nameOnLenguage, String temperature, String pressure, String humidity){
        System.out.println("**************************************************************");
        System.out.println(nameTown + " - " + nameOnLenguage);
        System.out.println(temperature + " - " + getInstance().concurrentHashMap.get("Temperature"));
        System.out.println(pressure + " - " + getInstance().concurrentHashMap.get("Pressure"));
        System.out.println(humidity + " - " + getInstance().concurrentHashMap.get("Humidity"));
        System.out.println("**************************************************************");
    }



}
